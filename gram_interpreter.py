#!/usr/bin/env python3

from collections import OrderedDict
import re
import sys
from typing import AnyStr, Tuple, Dict

from gram import GRAM

regex_comment = re.compile(r"#.*")
regex_label = re.compile(r"^[^\d]\w*$", re.ASCII)

regex_register = re.compile(r"^R(\d{1,2}$)", re.ASCII | re.IGNORECASE)
regex_const = re.compile(r"^(-?\d+)$", re.ASCII)
regex_ADR = re.compile(r"^(\d+)$", re.ASCII)
regex_ADR_ext = re.compile(r"^(\d+)\(R(\d{1,2})\)+$", re.ASCII | re.IGNORECASE)

# valid_commands = {END : 1, }
REG = "r"
ADR = "a"
ADR_EXT = "a(r)"
CONST = "m"

valid_commands = {"END": (),
                  "LOAD": (REG, ADR_EXT),
                  "STORE": (ADR_EXT, REG),
                  "C-LOAD": (REG, CONST),
                  "ADD": (REG, REG, REG),
                  "C-ADD": (REG, REG, CONST),
                  "SUB": (REG, REG, REG),
                  "MULT": (REG, REG, REG),
                  "DIV": (REG, REG, REG),
                  "MOD": (REG, REG, REG),
                  "SLT": (REG, REG, REG),
                  "JMP": (ADR),
                  "JAL": (ADR),
                  "BEQ": (REG, ADR),
                  "BNQ": (REG, ADR),
                  "SHL": (REG, REG),
                  "SHR": (REG, REG),
                  "ODD": (REG, REG),
                  "_P_REG": (REG),
                  "_P_MEM": (ADR),
                  "_P_REGB": (REG),
                  "_P_MEMB": (ADR),
                  "_P_REGX": (REG),
                  "_P_MEMX": (ADR),
                  }


def parse_labels(command_lines: Dict[int, AnyStr]) \
        -> (Dict[int, AnyStr], Dict[AnyStr, int]):
    """
    Parses labels from numbered code lines and removes them.

    :param command_lines: tokenized code lines
    :return: code lines with labels removed, and dictionary of labels
             and corresponding line numbers
    """
    labels = {}
    commands = {}

    for n, line in command_lines.items():
        if line[0].endswith(":"):
            label = line[0][:-1]

            if not label:
                raise ValueError(f"Empty label in line {n+1}!")

            if re.match(regex_label, label):
                commands[n] = line[1:]

                if label in labels.keys():
                    raise ValueError(f"Duplicate label '{label}'"
                                     f" in line {+1}!")

                labels[label] = n
            else:
                raise ValueError(f"Invalid label '{label}' in line {n+1}!")
        else:
            commands[n] = line

    return OrderedDict(commands), labels


def parse_commands(command_lines: Dict[int, AnyStr],
                   labels: Dict[AnyStr, int]) -> (Dict[int, AnyStr], Dict[str, int], int):
    """
    Parses commands from tokenized code lines, resolves labels and
    renumbers the resulting code lines. If a 'main' label is set, the
    PC will be set to the number of the labeled command.

    :param command_lines: tokenized code lines
    :param labels: dictionary of labels and their code lines
    :return: renumbered code with resolved labels, dictionary of labels
             with new line numbers and initial PC value.
    """
    assembled = []

    line_mapping = {}   # old line number: new command number

    sorted_line_ns = sorted(command_lines.keys())

    for i in range(len(command_lines)):
        line_mapping[sorted_line_ns[i]] = i

    # set labels correctly

    assembled_labels = {}

    for label, n in labels.items():
        assembled_labels[label] = line_mapping[n]

    if "main" in labels.keys():
        initial_pc = assembled_labels["main"]
    else:
        initial_pc = 0

    for n, line in command_lines.items():
        cmd = line[0]
        args = line[1:]

        if cmd not in valid_commands.keys():
            raise ValueError(f"Invalid command '{cmd}' in line {n+1}!")

        required_args = valid_commands[cmd]

        if len(args) != len(required_args):
            raise ValueError(f"Invalid argument count ({len(args)}) in line {n+1},"
                             f"'{cmd}' requires {required_args}!")

        new_cmd = [cmd]

        for i, req_arg in enumerate(required_args):
            arg = args[i]

            if req_arg == REG:
                match = re.search(regex_register, arg)

                if not match:
                    raise ValueError(
                        f"Invalid argument in line {n+1}, '{cmd}'"
                        f" requires {required_args},  {arg} given!"
                    )

                r = int(match.group(1))

                if (r < 0) or (r >= GRAM.REG_SIZE):
                    raise ValueError(
                        f"Invalid register in line {n+1}, only"
                        f" 0..{GRAM.REG_SIZE} possible!"
                    )

                new_cmd.append(r)
            elif req_arg == ADR:
                # first check for label
                if arg in labels.keys():
                    a = assembled_labels[arg]
                else:
                    match = re.search(regex_ADR, arg)

                    if not match:
                        raise ValueError(
                            f"Invalid argument in line {n+1}, '{cmd}' requires"
                            f" {required_args}, {arg} given!"
                        )

                    a = int(match.group(1))

                if (a < -GRAM.STACK_SIZE) or (a >= GRAM.HEAP_SIZE):
                    raise ValueError(
                        f"Invalid address in line {n+1}, only"
                        f" 0..{GRAM.HEAP_SIZE} or -1..-{GRAM.HEAP_SIZE}"
                        f" possible!"
                    )

                new_cmd.append(int(a))
            elif req_arg == ADR_EXT:
                ext_used = True

                match = re.search(regex_ADR_ext, arg)

                if not match:
                    ext_used = False
                    match = re.search(regex_ADR, arg)

                if not match:
                    raise ValueError(
                        f"Invalid argument in line {n+1}, '{cmd}' requires"
                        f" {required_args}, {arg} given!"
                    )

                a = int(match.group(1))
                r = 0

                if ext_used:
                    r = int(match.group(2))

                    if (r < 0) or (r >= GRAM.REG_SIZE):
                        raise ValueError(
                            f"Invalid register in line {n+1}, only"
                            f" 0..{GRAM.REG_SIZE} possible!"
                        )

                if arg in labels.keys():
                    a = assembled_labels[arg]

                if (a < -GRAM.STACK_SIZE) or (r >= GRAM.HEAP_SIZE):
                    raise ValueError(
                        f"Invalid address in line {n+1}, only"
                        f" 0..{GRAM.HEAP_SIZE} or -1..-{GRAM.HEAP_SIZE}"
                        f" possible!"
                    )

                new_cmd.append(int(a))
                new_cmd.append(int(r))
            elif req_arg == CONST:
                match = re.search(regex_const, arg)

                if not match:
                    raise ValueError(
                        f"Invalid argument in line {n+1}, '{cmd}' requires"
                        f" {required_args}, {arg} given!"
                    )

                m = int(match.group(1))

                new_cmd.append(int(m))
            else:
                raise ValueError("Internal error, unknown argument signature!")

        assembled.append(new_cmd)

    return assembled, assembled_labels, initial_pc


def print_commands(command_lines: Dict[int, AnyStr]) -> None:
    for n, line in command_lines.items():
        print(n, " ".join(line))


def assemble(file: str) -> (Dict[int, AnyStr], int):
    """
    Returns tokenized code from file and initial PC value.
    """
    with open(file) as f:
        content = f.readlines()

    content = [line.replace(",", " ") for line in content]

    command_lines = {}

    for i in range(len(content)):
        line = re.sub(regex_comment, "", content[i]).strip()

        if line:
            command_lines[i] = line.split()

    command_lines, labels = parse_labels(command_lines)
    code, _, pc = parse_commands(command_lines, labels)

    return code, pc


def create_gram(file: str, regs: Tuple[int] = (), heap: Tuple[int] = (),
                stack: Tuple[int] = ()) -> GRAM:
    """
    Creates a GRAM with given code, registers, heap and stack.
    """
    g = GRAM(regs, heap, stack)

    code, pc = assemble(file)
    g.load_code(code, pc)

    return g


if __name__ == "__main__":
    file = sys.argv[1]
    heap = tuple([int(arg) for arg in sys.argv[2:]])

    g = create_gram(file, heap=heap)
    g.run(print_cmd=True, interactive=False)

    print(g.get_heap(2))

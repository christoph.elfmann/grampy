# GRAMPy
This project is an interpreter for the "GRAM" ("Göttinger 
Registermaschine"), used in the course "Informatik III" at the 
Georg August University of Göttingen. See the lecture notes for more
details on it.

The interpreter is meant to be used interactively in a Python console.
It is able to execute GRAM assembler code and halt execution after
individual commands, so the user is able to inspect register and memory
contents. It supports comments and labels as well. If a 'main' 
label is set, the program execution starts from that point.

Example:
```
> import gram_interpreter
> g = gram_interpreter.create_gram("demo.gram", heap=[0, 42])
> g.print_heap(0, 3)
000	0
001	42
002	0

> g.print_registers(0, 4)
R0	0
R1	0
R2	0
R3	0

> g.print_code()
0: LOAD     1, 1, 0
1: C-LOAD   2, 15
2: ADD      3, 1, 2
3: STORE    2, 0, 3
4: END

> g.run(interactive=True)
<GRAM: Running.>
<0: LOAD     1, 1, 0>>? 
<1: C-LOAD   2, 15>>? q
<GRAM: Stopped.>

> g.print_registers(0, 4)
R0	0
R1	42
R2	15
R3	0

> g.run(print_cmd=True)
<GRAM: Running.>
<2: ADD      3, 1, 2>
<3: STORE    2, 0, 3>
<4: END      >
<GRAM: Done.>

> g.print_heap(0, 3)
000	0
001	42
002	57

> g.reset()
> g.print_heap(0, 3)
000	0
001	42
002	0

> g.print_registers(0, 4)
R0	0
R1	0
R2	0
R3	0
```

Use `create_gram` from `gram_interpreter.py` to create a GRAM object.
See the documentation in `gram.py` for more commands.

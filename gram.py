from math import ceil, floor, log10
from typing import List, Tuple, Union, Optional


class GRAM:
    """
    Representation of a GRAM ("Göttinger Registermaschine"), including
    registers, stack and heap memory, and the ability to execute GRAM
    pseudo assembler code, which needs to be processed by the
    `gram_interpreter.py` beforehand.
    """

    REG_SIZE = 32
    HEAP_SIZE = 256
    STACK_SIZE = 256

    HP = 28  # heap pointer
    FP = 29  # frame pointer
    SP = 30  # stack pointer
    RA = 31  # return address

    def __init__(self, regs: Tuple[int] = (), heap: Tuple[int] = (),
                 stack: Tuple[int] = ()):
        # note: register 0 must always be zero
        if len(regs) >= GRAM.REG_SIZE:
            raise ValueError(f"GRAM: too many heap values"
                             f"(> {GRAM.HEAP_SIZE-1})!")

        if len(heap) > GRAM.HEAP_SIZE:
            raise ValueError(f"GRAM: too many heap values"
                             f"(> {GRAM.HEAP_SIZE})!")

        if len(stack) > GRAM.STACK_SIZE:
            raise ValueError(f"GRAM: too many stack values"
                             f"(> {GRAM.STACK_SIZE})!")

        self.initial_pc = 0
        self.pc: int = self.initial_pc
        self.code: List[List[str]] = [["END"]]
        self._pc_digits = ceil(log10(len(self.code)))
        self.state: str = "ready"  # ready, running, stopped, done
        self.cmd_counter = 0

        self.initial_regs: List[int] = [0] * GRAM.REG_SIZE
        self.initial_heap: List[int] = [0] * GRAM.HEAP_SIZE
        self.initial_stack: List[int] = [0] * GRAM.STACK_SIZE

        for i, val in enumerate(regs):
            self.initial_regs[i + 1] = int(val)

        for i, val in enumerate(heap):
            self.initial_heap[i] = int(val)

        for i, val in enumerate(stack):
            self.initial_stack[i] = int(val)

        self.regs = self.initial_regs.copy()
        self.heap = self.initial_heap.copy()
        self.stack = self.initial_stack.copy()

    def load_code(self, code: List[List[Union[str, int]]] = None, pc: int = 0) \
            -> None:
        """
        Loads tokenized code and sets the PC.

        :param code: tokenized code (list of statements, where each
                     statement is a list containing command and
                     arguments)
        :param pc: program counter value
        """
        self.code = code if code else [["END"]]
        self._pc_digits = ceil(log10(len(self.code)))
        self.initial_pc = pc
        self.pc = pc
        self.state = "ready"
        self.cmd_counter = 0

    def run(self, steps: int = -1, print_cmd: bool = False,
            interactive: bool = False) -> None:
        """
        Runs the code starting from the current PC.
        If in interactive mode, the program halts after the execution
        of each statement, and the user can suspend the program
        by typing anything and hitting enter.

        :param steps: number of statements executed (-1 for non-stop
                      execution)
        :param print_cmd: if true, print current statement
        :param interactive: if true, run in interactive mode
        """
        self.state = "running"

        if print_cmd or interactive:
            print(f"<GRAM: Running.>")

        while (((steps > 0) or (steps == -1))
               and (self.state != "done") and (self.state != "stopped")):
            if (self.pc < 0) or (self.pc >= len(self.code)):
                raise ValueError(f"GRAM: Program counter out of range"
                                 f" ({self.pc})!")

            line = self.code[self.pc]

            if len(line) == 0:
                raise ValueError("GRAM: Empty command!")

            str_cmd = line[0]
            args = line[1:]

            self._check_args(str_cmd, args)

            cmd = self.commands[str_cmd][0]

            if print_cmd or interactive:
                statement = GRAM._cmd_to_string(self.pc, line[0], line[1:],
                                                pc_width=self._pc_digits)
                print(f"<{statement}>", end="")

            self.pc += 1
            cmd(self, args)

            self.cmd_counter += 1

            if steps > 0:
                steps -= 1

            if interactive:
                if self.state != "done":
                    v = input("")

                    if v != "":
                        self.state = "stopped"
                else:
                    print("")
            elif print_cmd:
                print("")

        if self.state != "stopped":
            self.state = "done"

        if print_cmd or interactive:
            if self.state == "done":
                print(f"<GRAM: Done.>")
            elif self.state == "stopped":
                print(f"<GRAM: Stopped.>")

    def reset(self) -> None:
        """
        Resets the GRAM, including PC, command counter, state, heap,
        stack and registers.
        """
        self.pc = self.initial_pc
        self.cmd_counter = 0
        self.state: str = "ready"
        self.heap = self.initial_heap.copy()
        self.stack = self.initial_stack.copy()
        self.regs = self.initial_regs.copy()

    def _check_args(self, cmd: str, args: List[str]) -> None:
        """
        Raises a ValueError if the command or the number of supplied
        arguments in incorrect.
        """
        if cmd in self.commands.keys():
            args_provided = len(args)

            args_needed = self.commands[cmd][1]

            if args_needed != args_provided:
                raise ValueError(f"GRAM: Invalid argument count"
                                 f"({args_provided}), {args_needed} needed!")
        else:
            raise ValueError(f"GRAM: Invalid command '{cmd}'!")

    @staticmethod
    def _cmd_to_string(pc: int, cmd: str, args: List[str],
                       pc_width: int = 4, cmd_width: int = 8) -> str:
        """
        Returns string representation of statement.
        """
        argument_string = ', '.join([str(arg) for arg in args])
        return f"{pc:>{pc_width}}: {cmd:<{cmd_width}} {argument_string}"

    def _code_to_string(self) -> str:
        """
        Returns string representation of loaded code.
        """
        commands = [GRAM._cmd_to_string(i, line[0], line[1:],
                                        pc_width=self._pc_digits)
                    for i, line in enumerate(self.code)]
        return "\n".join(commands) + "\n"

    def print_code(self):
        print(self._code_to_string())

    def set_reg(self, reg: int, val: int):
        """ Set register to value. """
        if not 1 <= reg < GRAM.REG_SIZE:
            raise ValueError(f"GRAM: Register {reg} out of bounds!")

        self.regs[reg] = val

    def get_reg(self, reg: int) -> int:
        """ Get register value. """
        if not 0 <= reg < GRAM.REG_SIZE:
            raise ValueError(f"GRAM: Register {reg} out of bounds!")

        return self.regs[reg]

    def set_mem(self, adr: int, val: int):
        """ Set stack or heap memory value. """
        if val >= 0:
            self.set_heap(adr, val)
        else:
            self.set_stack(-(adr + 1), val)

    def get_mem(self, adr: int) -> int:
        """ Get stack or heap memory value. """
        if adr >= 0:
            return self.get_heap(adr)
        else:
            return self.get_stack(-(adr + 1))

    def set_heap(self, adr: int, val: int):
        """ Set heap value. """
        if not 0 <= adr < GRAM.HEAP_SIZE:
            raise ValueError(f"GRAM: Heap address {adr} out of bounds!")

        self.heap[adr] = val

    def get_heap(self, adr: int) -> int:
        """ Get heap value. """
        if not 0 <= adr < GRAM.HEAP_SIZE:
            raise ValueError(f"GRAM: Heap address {adr} out of bounds!")

        return self.heap[adr]

    def set_stack(self, adr: int, val: int):
        """ Set stack value. """
        if not 0 <= adr < GRAM.STACK_SIZE:
            raise ValueError(f"GRAM: Stack address {adr} out of bounds!")

        self.stack[adr] = val

    def get_stack(self, adr: int) -> int:
        """ Get stack value. """
        if not 0 <= adr < GRAM.STACK_SIZE:
            raise ValueError(f"GRAM: Stack address {adr} out of bounds!")

        return self.stack[adr]

    @staticmethod
    def _get_slice(start: Optional[int], end: Optional[int],
                   max_size: int) -> Tuple[int, int]:
        if start is None:
            start = 0
            end = max_size
        elif end is None:
            end = start + 1

        return tuple(sorted((start, end)))

    def print_registers(self, start: Optional[int] = None,
                        end: Optional[int] = None) -> None:
        """
        Print all, one or range of register contents.

        :param start: start index; if None, print all
        :param end: end index (exclusive); if None print content
                    at start index
        """
        for i in range(*self._get_slice(start, end, GRAM.REG_SIZE)):
            print(f"R{i}\t{self.regs[i]}")

    def print_stack(self, start: Optional[int] = None,
                    end: Optional[int] = None) -> None:
        """
        Print all, one or range of stack cells.

        :param start: start index; if None, print all
        :param end: end index (exclusive); if None print content
                    at start index
        """
        if start is not None:
            start = -start - 1

        if end is not None:
            end = -end - 1

        num_digits = ceil(log10(GRAM.HEAP_SIZE)) + 1

        for i in range(*self._get_slice(start, end, GRAM.STACK_SIZE)):
            print(f"{-(i+1):0{num_digits}}\t{self.stack[i]}")

    def print_heap(self, start: Optional[int] = None,
                   end: Optional[int] = None) -> None:
        """
        Print all, one or range of heap cells.

        :param start: start index; if None, print all
        :param end: end index (exclusive); if None print content
                    at start index
        """
        num_digits = ceil(log10(GRAM.HEAP_SIZE))

        for i in range(*self._get_slice(start, end, GRAM.HEAP_SIZE)):
            print(f"{i:0{num_digits}}\t{self.heap[i]}")

    # see a complete description of the commands in the lecture notes
    def _cmd_end(self, args: List[str]) -> None:
        self.state = "done"

    def _cmd_load(self, args: List[str]) -> None:
        r1, a, r2 = args
        self.set_reg(r1, self.get_mem(a + r2))

    def _cmd_store(self, args: List[str]) -> None:
        a, r1, r2 = args
        self.set_mem(a + r1, self.get_reg(r2))

    def _cmd_cload(self, args: List[str]) -> None:
        r1, m = args
        self.set_reg(r1, m)

    def _cmd_add(self, args: List[str]) -> None:
        r1, r2, r3 = args
        self.set_reg(r1, self.get_reg(r2) + self.get_reg(r3))

    def _cmd_cadd(self, args: List[str]) -> None:
        r1, r2, m = args
        self.set_reg(r1, self.get_reg(r2) + m)

    def _cmd_sub(self, args: List[str]) -> None:
        r1, r2, r3 = args
        self.set_reg(r1, self.get_reg(r2) - self.get_reg(r3))

    def _cmd_mult(self, args: List[str]) -> None:
        r1, r2, r3 = args
        self.set_reg(r1, self.get_reg(r2) * self.get_reg(r3))

    def _cmd_div(self, args: List[str]) -> None:
        r1, r2, r3 = args
        self.set_reg(r1, floor(self.get_reg(r2) / self.get_reg(r3)))

    def _cmd_mod(self, args: List[str]) -> None:
        r1, r2, r3 = args
        mod = self.get_reg(r2) \
              - floor(self.get_reg(r2) / self.get_reg(r3)) * self.get_reg(r3)
        self.set_reg(r1, mod)

    def _cmd_slt(self, args: List[str]) -> None:
        r1, r2, r3 = args

        if self.get_reg(r2) < self.get_reg(r3):
            self.set_reg(r1, 1)
        else:
            self.set_reg(r1, 0)

    def _cmd_jmp(self, args: List[str]) -> None:
        a, = args
        self.pc = a

    def _cmd_jal(self, args: List[str]) -> None:
        a, = args
        self.set_reg(GRAM.RA, self.pc)
        self.pc = a

    def _cmd_beq(self, args: List[str]) -> None:
        r, a = args

        if self.get_reg(r) == 0:
            self.pc = a

    def _cmd_bnq(self, args: List[str]) -> None:
        r, a = args
        if self.get_reg(r) != 0:
            self.pc = a

    def _cmd_shl(self, args: List[str]) -> None:
        r1, r2 = args
        self.set_reg(r1, self.get_reg(r2) * 2)

    def _cmd_shr(self, args: List[str]) -> None:
        r1, r2 = args
        self.set_reg(r1, floor(self.get_reg(r2) / 2))

    def _cmd_odd(self, args: List[str]) -> None:
        r1, r2 = args
        self.set_reg(r1, self.get_reg(r2) % 2)

    def _cmd_preg(self, args: List[str]) -> None:
        r, = args
        print(f"R{r}: {self.get_reg(r)}")

    def _cmd_pmem(self, args: List[str]) -> None:
        a, = args

        if a >= 0:  # heap
            print(f"HEAP({a}): {self.heap[a]}")
        else:  # stack
            a = -a - 1
            print(f"STACK({a}): {self.stack[a]}")

    def _cmd_pregb(self, args: List[str]) -> None:
        r, = args
        print(f"R{r}: {bin(self.get_reg(r))}")

    def _cmd_pmemb(self, args: List[str]) -> None:
        a, = args

        if a >= 0:  # heap
            print(f"HEAP({a}): {bin(self.heap[a])}")
        else:  # stack
            a = -a - 1
            print(f"STACK({a}): {bin(self.stack[a])}")

    def _cmd_pregx(self, args: List[str]) -> None:
        r, = args
        print(f"R{r}: {hex(self.get_reg(r))}")

    def _cmd_pmemx(self, args: List[str]) -> None:
        a, = args

        if a >= 0:  # heap
            print(f"HEAP({a}): {hex(self.heap[a])}")
        else:  # stack
            a = -a - 1
            print(f"STACK({a}): {hex(self.stack[a])}")

    # extended addresses are expanded
    commands = {"END": (_cmd_end, 0),
                "LOAD": (_cmd_load, 3),
                "STORE": (_cmd_store, 3),
                "C-LOAD": (_cmd_cload, 2),
                "ADD": (_cmd_add, 3),
                "C-ADD": (_cmd_cadd, 3),
                "SUB": (_cmd_sub, 3),
                "MULT": (_cmd_mult, 3),
                "DIV": (_cmd_div, 3),
                "MOD": (_cmd_mod, 3),
                "SLT": (_cmd_slt, 3),
                "JMP": (_cmd_jmp, 1),
                "JAL": (_cmd_jal, 1),
                "BEQ": (_cmd_beq, 2),
                "BNQ": (_cmd_bnq, 2),
                "SHL": (_cmd_shl, 2),
                "SHR": (_cmd_shr, 2),
                "ODD": (_cmd_odd, 2),
                "_P_REG": (_cmd_preg, 1),
                "_P_MEM": (_cmd_pmem, 1),
                "_P_REGB": (_cmd_pregb, 1),
                "_P_MEMB": (_cmd_pmemb, 1),
                "_P_REGX": (_cmd_pregx, 1),
                "_P_MEMX": (_cmd_pmemx, 1),
                }
